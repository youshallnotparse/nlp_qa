from websocket import create_connection

DELIM_STRING = "__!@#$%^&*()__"

class FSE:
    
    def __init__(self,port):
        self.wsPort = port
        self.server_address = "ws://localhost:"+str(self.wsPort)+"/"
        self.ws = create_connection(self.server_address) 
        
    def split(self,sentence):
        self.ws.send(sentence)
        ans = []
        s = self.ws.recv()
        while s != DELIM_STRING:
            ans.append(s)
            s = self.ws.recv()
        return ans