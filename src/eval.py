'''
TODO List for Answer Module:
[ ] Parsing wiki pages, cleaning and generating text
[X] Basic framework for answering an easy question
[ ] Negation - Answer (i.e. text sentence conatins 'not' etc.)
[ ] Negation - Question (i.e. question conatins 'not' etc.)
[ ] Answers for Medium Questions
[ ] Co-reference resolution
[ ] Handling synonyms etc.


Things to check:
1. How many candidates to check for ?
2. Experimented with the lemmatizer but it does not work for ate , did , does ...
3. The pos tagger was tagging John as JJ (so stupid ...)
'''

import sys
import constants
from nltk import sent_tokenize
from collections import Counter
from question import buildModules
from candidateAnswers import *
from easyAnswer import *
from answer import *
from configureEnvironment import prepareEnvironment


def main(argv):
    prepareEnvironment()
    parseModules = buildModules()
    # reset the encoding to utf8
    reload(sys)
    sys.setdefaultencoding('utf8')
    cur=""
    
    path = '../data/Evaluation_Data/S10/'
    with open('../data/Evaluation_Data/S10/question_answer_pairs.txt') as f:
        qa_pairs = f.read().splitlines()
    for line in qa_pairs[1:]:
        line = line.split('\t')
        if(len(line)<6):
            continue
        if(line[3].lower()!='medium'):
            continue
        articlePath = line[5]
        question = line[1]
        answer = [2]
        text = read_file(path+articlePath+'.txt')
        sents = text_to_sents(text)
        word_vecs = sents_to_wordvecs(sents)
        unwanted_indices = [i for i in xrange(len(word_vecs)) if len(word_vecs[i]) > 3]
        sents = [sents[i] for i in xrange(len(sents)) if i in unwanted_indices]
        word_vecs = [word_vecs[i] for i in xrange(len(word_vecs)) if i in unwanted_indices]

        questions = [formatize(question)]#read_file('../data/q1.txt').split('\n')
        qword_vecs = sents_to_wordvecs(questions)

        text = sent_tokenize(formatize(text))
        for question in questions:
            if(question.split()[0].lower()!='how'):
                pass
            if(question.split()[0].lower() != 'why'):
                continue
            if(question==cur):
                continue
            cur=question
            print 'Question:', question
            q_type, q_head = question_type(question,parseModules)
            question = formatize(question)
            i = questions.index(question)
            candidates, cand_word_vecs = find_answer_candidates_boolean(sents, question, word_vecs, qword_vecs[i])
            if(q_type):
                answer_easy_question(candidates, question)
            else:
                if(q_head.lower()=='how'):
                    #print answer
                    print "Answer : "+str(answer_how_m_question(candidates,question,parseModules))
                if(q_head.lower() == 'why'):
                    #print answer
                    print "Answer : "+str(answer_why_question(candidates,question,parseModules))
            print "\n\n"


if __name__ == "__main__":
    main(sys.argv)




