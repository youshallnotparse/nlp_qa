'''
TODO List for Answer Module:
[ ] Parsing wiki pages, cleaning and generating text
[X] Basic framework for answering an easy question
[ ] Negation - Answer (i.e. text sentence conatins 'not' etc.)
[ ] Negation - Question (i.e. question conatins 'not' etc.)
[ ] Answers for Medium Questions
[ ] Co-reference resolution
[ ] Handling synonyms etc.


Things to check:
1. How many candidates to check for ?
2. Experimented with the lemmatizer but it does not work for ate , did , does ...
3. The pos tagger was tagging John as JJ (so stupid ...)
'''

import sys
import constants
from nltk import sent_tokenize
from collections import Counter
from question import buildModules
from candidateAnswers import *
from easyAnswer import *
from mediumAnswer import *
from configureEnvironment import prepareEnvironment
import time


def read_file(filename):    
    file = open(filename, 'r')
    text = unicode(file.read(), errors='replace')
    return text.encode("ascii", "ignore")


def main(argv):
    prepareEnvironment()
    parseModules = buildModules(True)
    # reset the encoding to utf8
    reload(sys)
    sys.setdefaultencoding('utf8')
    
    if (len(sys.argv) == 3):
        article_file = sys.argv[1]
        question_file = sys.argv[2]
    else:
        print "Not enough arguments :("
        exit(0)
        
    try:
        text = read_file(article_file)
        sents = text_to_sents(text)
        word_vecs = sents_to_wordvecs(sents)
        unwanted_indices = [i for i in xrange(len(word_vecs)) if len(word_vecs[i]) > 3]
        sents = [sents[i] for i in xrange(len(sents)) if i in unwanted_indices]
        word_vecs = [word_vecs[i] for i in xrange(len(word_vecs)) if i in unwanted_indices]
        #print len(sents), len(word_vecs), max(unwanted_indices)

        questions = read_file(question_file).split("\n")
        questions = [q for q in questions if len(q) > 0]
        qword_vecs = sents_to_wordvecs(questions)
    except:
        print "Error reading/processing article/questions file."
        exit(0)
    
    for i in xrange(len(questions)):
        question = questions[i]
        #print 'Question:', question
        try:
            q_type, q_head = question_type(question,parseModules)
            candidates, cand_word_vecs = find_answer_candidates_boolean(sents, question, word_vecs, qword_vecs[i])
        except:
            print "Unable determine question type."
            continue
        if(q_type):
            try:
                ans = answer_easy_question(cand_word_vecs, qword_vecs[i])
                print ans
            except:
                print "YES"
                continue
        else:
            try:
                if(q_head.lower()=='where'):
                    print answer_where_question(candidates,question,parseModules)
                elif(q_head.lower()=='when'):
                    print answer_when_question(candidates,question,parseModules)
                elif(q_head.lower()=='who' or q_head.lower()=='whom'):
                    print answer_who_question(candidates,question,parseModules)
                elif(q_head.lower()=='why'):
                    print answer_why_question(candidates,question,parseModules)
                elif(q_head.lower()=='what'):
                    print answer_what_question(candidates,question,parseModules)
                elif(q_head.lower()=='whose'):
                    print answer_whose_question(candidates, question, parseModules)
                elif 'how' in question.lower():
                    print answer_how_m_question(candidates, question, parseModules)
                else:
                    if len(candidates) > 0:
                        print candidates[0]
                    else:
                        print sents[0]
            except:
                if len(candidates) > 0:
                    print candidates[0]
                else:
                    print sents[0]
if __name__ == "__main__":
    main(sys.argv)




