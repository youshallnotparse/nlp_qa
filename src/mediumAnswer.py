import constants
from nltk import word_tokenize
from question import buildModules
from candidateAnswers import *
from parseTrees import *
from easyAnswer import *
from easyQues import getSubjectVerbs


def answer_when_question(candidates, question, modules):
    parser = modules['parser']
    depParser = modules['depParser']
    lem = modules['lemmatizer']
    depTriplets = [list(parse.triples()) for parse in depParser.parse(word_tokenize(question))] 
    mainVerb, _, subject = getSubjectVerbs(depTriplets)
    mainVerb, subject = mainVerb[0], subject[0]
    mainVerb = lem.lemmatize(mainVerb, 'v')
    ans = None
    for candidate in candidates:
        tokens = word_tokenize(candidate)
        candParseTree = lemmatize_tree(parser.parse(tokens).next()[0], lem)
        cr1 = get_subtree(candParseTree, [subject, mainVerb])
        cr2 = get_child(cr1, "VP", mainVerb)
        if cr2 == None:
            cr2 = get_subtree(candParseTree, [mainVerb])
            if cr2 == None:
                cr2 = candParseTree
        time_preps = ["on", "in", "at", "during", "until"]
        for prep in time_preps:
            cr3 = get_child(cr2, "PP", prep)
            if cr3 != None:
                break
        cr4 = get_child_index(cr3, 1)
        if cr4 == None:
            cr5 = get_child(candParseTree, "PP")
            cr6 = get_child_index(cr5, 1)
            if cr6 != None:
                return ' '.join(cr6.leaves())
        else:
            return ' '.join(cr4.leaves())
    # if not answerable return the first candidate 
    return candidates[0]


def answer_whose_question(candidates, question, modules):
    parser = modules['parser']
    depParser = modules['depParser']
    
    for candidate in candidates:    
        tags =  modules['ssTagger'].tag(candidate)
        prev_tag = None
        for tag in tags:
            (word, pos, info) = tag
            if prev_tag != None and ("'" == word or "'s" == word):
                (pword, ppos, pinfo) = prev_tag
                return pword + word
            prev_tag = tag
                
    return candidates[0]


def answer_how_m_question(candidates, question, modules):
    parser = modules['parser']
    depParser = modules['depParser']
    lem = modules['lemmatizer']
    
    for candidate in candidates:    
        #print candidate
        tokens = word_tokenize(candidate)
        candParseTree = lemmatize_tree(parser.parse(tokens).next()[0], lem)
        #candParseTree.pretty_print()
        cr1 = get_sub_child(candParseTree, "NP", "QP")
        if cr1 == None:
            cr1 = get_sub_child(candParseTree, "NP", "CD")
        if cr1 != None:
            return ' '.join(cr1.leaves())

    return candidates[0]
        