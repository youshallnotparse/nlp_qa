SUBJECT_STRING = u'subj'
DOBJECT_STRING = u'dobj'
VERB_STRING = u'VB'
COP_STRING = u'cop'
AUX_STRING = u'aux'
CASE_STRING = u'case'
NP_STRING = u'NP'
PP_STRING = u'PP'
SBAR_STRING = u'SBAR'
VP_STRING = u'VP'
PROPER_NOUN_STRING = u'NNP'
PERSONAL_PRONOUN_STRING = u'PRP'
PREPOSITION_STRING = u'IN'
TO_STRING = u'TO'
ADJECTIVE_STRING = u'JJ'
NNS_STRING = u'NNS'
NNPS_STRING = u'NNPS'

QMARK = u'?'
PRESENT_VERB_FORMS = [u'VB',u'VBZ',u'VBP',u'VBG']

NER_PERSON = u'person'
NER_LOCATION = u'location'
NER_ORG = u'organization'

UNMV_STRING = "unmv"

BE_BASE = u'be'
DO_SUBJECTS = [u'i',u'we',u'they',u'you'] 
PLURAL_NOUNS = {NNS_STRING,NNPS_STRING}

EASY_QUESTION = [u'SQ', u'SINV']

WHAT_STRING = u'what'
WHO_STRING = u'who'
WHEN_STRING = u'when'
WHERE_STRING = u'where'

TAG_PERSON = 'person'
TAG_TIME = 'time'
TAG_LOCATION = 'location'

SS_TAGS = {
            'person' : TAG_PERSON,
            'group' : TAG_PERSON,
            'time' : TAG_TIME,
            'location' : TAG_LOCATION
           }
