import re
import string
import constants
import math
import numpy as np
from nltk import word_tokenize 
from collections import Counter
from scipy.sparse import csr_matrix
from nltk import word_tokenize, sent_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from pattern.en import singularize


def create_vocab(word_vecs):
    words = []
    for vec in word_vecs:
        for word in vec:
            words.append(word)
    return set(words)


def add_to_vocab(vocab, word_vec):
    for vec in word_vec:
        for word in vec:
            vocab.add(word)
    return list(vocab)


def text_to_sents(text):
    return sent_tokenize(formatize(text))


def sents_to_wordvecs(sents):
    word_vecs = []
    for sent in sents:
        word_vecs.append(word_tokenize(formatize(sent)))
    return formatize_wordvecs(word_vecs)


def formatize_wordvecs(word_vecs):
    stop_words = set(stopwords.words('english'))
    stop_words.add("true")
    pattern = re.compile("["+string.punctuation+"]")
    for i in xrange(len(word_vecs)):
        for j in xrange(len(word_vecs[i])):
            word_vecs[i][j] = re.sub("'s", "", word_vecs[i][j])
            word_vecs[i][j] = re.sub(pattern, "", word_vecs[i][j]).lower()
            word_vecs[i][j] = singularize(word_vecs[i][j])
            
    for i in xrange(len(word_vecs)):
        word_vecs[i] = [word for word in word_vecs[i] if len(word) > 0 and word not in stop_words]
    return [transform(word_vec) for word_vec in word_vecs]


def build_vocab(text, question):
    words = []
    for sentence in text:
        #print sentence
        words.extend(remove_stopwords(word_tokenize(sentence)))
    #print words
    words.extend(remove_stopwords(word_tokenize(question)))
    words.remove(constants.QMARK)
    #words.remove(",")
    words = list(set(words))
    return [word.lower() for word in words]


def remove_stopwords(tokens):
    stop_words = set(stopwords.words('english'))
    other_words = ["true", ","]
    new_tokens = [token for token in tokens if token not in stop_words]
    new_tokens = [token for token in tokens if token not in other_words]
    new_tokens = [new_token.lower() for new_token in new_tokens]
    return new_tokens


def find_answer_candidates_boolean(text, question, word_vecs, qword_vec):
    vocab = create_vocab(word_vecs)
    vocab = add_to_vocab(vocab, [qword_vec])
    cand_indx = []
    qset = set(qword_vec)
    for i in xrange(len(text)):
        cset = set(word_vecs[i])
        score = len(cset.intersection(qset))
        cand_indx.append((i+1, score))
    cand_indx = sorted(cand_indx, key=lambda x: (x[1], -x[0]), reverse=True)
    max_cands = 10
    cand_indx = cand_indx[0:max_cands]
    cand_indx = [indx[0]-1 for indx in cand_indx]
    return [text[i] for i in cand_indx], [word_vecs[i] for i in cand_indx]


def find_answer_candidates(text, question, word_vecs, qword_vec):
    vocab = create_vocab(word_vecs)
    vocab = add_to_vocab(vocab, [qword_vec])    
    inverted_index = build_inverted_index(text, vocab)
    
    n = len(text)
    r = 0
    row, col, data = [], [], []
    
    for word_vec in word_vecs:
        count_dict = Counter(transform(word_vec))
        for i in xrange(len(vocab)):
            if vocab[i] in count_dict:
                row.append(r)
                col.append(i)
                try:
                    idf = math.log(float(n)/(1+inverted_index[vocab[i]]))
                except:
                    continue
                data.append(count_dict[vocab[i]]*idf)
        r += 1        

    sent_features = csr_matrix((data, (row, col)), shape=(n, len(vocab)))

    row, col, data = [], [], []
    count_dict = Counter(transform(qword_vec))
    
    for i in xrange(len(vocab)):
        if vocab[i] in count_dict:
            row.append(0)
            col.append(i)
            data.append(count_dict[vocab[i]])
    ques_features = csr_matrix((data, (row, col)), shape=(1, len(vocab)))
    sent_features = (sent_features ).toarray()
    ques_features = (ques_features ).toarray()
    matches = np.dot(sent_features, ques_features.T)
    threshold = 1.0
    can_idx = []

    while len(can_idx) == 0:
        can_idx = np.argwhere(matches >= threshold)
        can_idx = [(idx[0], matches[idx[0]]) for idx in can_idx]
        threshold /= 4
  
    can_idx = sorted(can_idx, key=lambda x: x[1], reverse=True)
    #print [text[idx[0]] for idx in can_idx]
    for i in xrange(len(word_vecs)):
        pass
        #print word_vecs[i], matches[i]
    return [text[idx[0]] for idx in can_idx], [word_vecs[idx[0]] for idx in can_idx]


def formatize(text, complete=False):
    '''
    1. Strip off 's and '
    2. Remove extra/unwanted chars
    3. Convert to lower case 
    
    TODO :
    1. Replace surnames with full names ??? 
    '''
    patterns = ["\n+", "\.\.+", "/'.*/", "\"", "[()]", "-.RB-"]
    repl_strings = [". ", ".", "", "", "", ""]
    if complete:
        patterns.append("["+string.punctuation+"]")
        repl_strings.append("")
    for (p, s) in zip(patterns, repl_strings):
        text = re.sub(re.compile(p), s, text)
    return text


def transform(tokens):
    wnl = WordNetLemmatizer()
    verb_lemms = [wnl.lemmatize(token, 'v') for token in tokens]
    #print "verbs", verb_lemms
    tokens = [verb_lemms[i] if verb_lemms[i] != tokens[i] \
              else wnl.lemmatize(tokens[i]) \
              for i in xrange(len(tokens))]
    #print "Final", tokens
    return tokens
    

def get_verbs(dep_triplets):
    verbs = [t[0][0] for t in dep_triplets[0] if constants.VERB_STRING in t[0][1] and t[0][0]]
    return remove_stopwords(set(verbs))


def question_type(question, modules):
    parser = modules['parser']
    tokens = word_tokenize(question)
    parseTree = parser.parse(tokens).next()
    parseTree = parseTree[0]
    if(parseTree.label() in constants.EASY_QUESTION):
        return 1,None
    else:
        return 0,parseTree.leaves()[0]


def build_inverted_index(text, vocab):
    inverted_index = dict.fromkeys(vocab, 0)
    inverted_index = Counter(inverted_index)
    for word_vec in text:
        count_dict = Counter(word_vec)
        inverted_index.update(count_dict.keys())
    return inverted_index
