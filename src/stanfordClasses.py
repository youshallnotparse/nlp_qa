import nltk
from nltk.tag import StanfordNERTagger
from nltk.tag import StanfordPOSTagger
from nltk.parse.stanford import StanfordParser, StanfordDependencyParser
from nltk.internals import find_jars_within_path

def handleJars(st):
    stanford_dir = st._stanford_jar.rpartition('/')[0]
    stanford_jars = find_jars_within_path(stanford_dir)
    st._stanford_jar = ':'.join(stanford_jars)

def handleParserjars(st):
    stanford_dir = st._classpath[0].rpartition('/')[0]
    stanford_jars = find_jars_within_path(stanford_dir)
    st._classpath = (':'.join(stanford_jars),st._classpath[1])
    
def getStanfordNERTagger():
    st = StanfordNERTagger('../tools/stanford-ner/classifiers/english.all.3class.distsim.crf.ser.gz')
    handleJars(st)
    return st

def getStanfordPOSTagger():
    st = StanfordPOSTagger('../tools/stanford-postagger/models/english-bidirectional-distsim.tagger')
    handleJars(st)
    return st

def getStanfordParser():
    st = StanfordParser()
    handleParserjars(st)
    return st

def getStanfordDependencyParser():
    st = StanfordDependencyParser()
    handleParserjars(st)
    return st
