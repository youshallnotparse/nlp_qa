import os 
CONFIG_PATH = "config.txt"
GLOBAL_PROPS = {}

def prepareEnvironment(): 
    global GLOBAL_PROPS
    props = buildProperties()
    GLOBAL_PROPS = props
    classPath  = props['nerPath'] + ":" + props['posPath'] + ":" + props['parserPath'] + ":" + props['parserModelPath']
    modelsPath = props['nerModelPath'] + ":" + props['posModelPath'] + ":" + props['parserModelPath']
    os.environ['CLASSPATH'] = classPath
    os.environ['STANFORD_MODELS'] = modelsPath
    return
    
def buildProperties():
    with open(CONFIG_PATH,'r') as f:
        lines = f.readlines()
    
    props = {}
    for l in lines:
        l = l.strip()
        if l == '':
            continue
        tokens = l.split('=')
        props[tokens[0]] = tokens[1]
    
    return props

def getGlobalProps():
    global GLOBAL_PROPS
    return GLOBAL_PROPS
