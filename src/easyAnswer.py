import constants
from nltk import word_tokenize
from question import buildModules
from candidateAnswers import *
from pattern.en import parsetree
from easyQues import getSubjectVerbs
from nltk.tree import Tree
from nltk.stem import WordNetLemmatizer
from candidateAnswers import remove_stopwords


def answer_easy_question(cand_word_vecs, qword_vec):
    for candidate in cand_word_vecs:
        flag = True
        for token in qword_vec:
            if token not in candidate:
                flag = False
                break
        if flag:
            return "YES"
    return "NO"


def answer_where_question(candidates,question,modules):

    parser = modules['parser']
    depParser = modules['depParser']
    lem = modules['lemmatizer']

    #process question
    qParseTree = parser.parse(word_tokenize(question)).next()[0]

    depTriplets = [list(parse.triples()) for parse in depParser.parse(word_tokenize(question))] 
    qParseTree = lemmatize_tree(qParseTree,lem)
    [mainVerb,auxVerbs,subject] = getSubjectVerbs(depTriplets)
    mainVerb = list(mainVerb)
    mainVerb[0] = lem.lemmatize(mainVerb[0].lower(),'v')

    subject=list(subject)
    #subject[0]=subject[0].lower()

    #process answers, and choose the one that has the most number of matching terms with the question. Then find answer within.
    ansParseTree,winner = find_winning_candidate(candidates,qParseTree,parser,lem)
    #print "Winner : "+winner

    #tagged =  modules['ssTagger'].tag(withininner)
    minH = 1000000
    subjectTree = None
    for t in ansParseTree.subtrees(lambda t: mainVerb[0] in t.leaves() and subject[0] in t.leaves()):
        if(t.height() < minH):
            minH = t.height()
            subjectTree = t
    if(subjectTree==None):
        return winner
    maxH = 100000

    verbTree = None

    for t in subjectTree.subtrees(lambda t : mainVerb[0] in t.leaves() and t.label()=='VP'):
        if(t.height() < maxH):
            maxH = t.height()
            verbTree = t 
    if(verbTree==None):
        verbTree = subjectTree
    ans = []
    for t in verbTree.subtrees(lambda t:t.label()=='PP'):
        #check theta the preposition is in list
        # output the prepositional phrase till the locations stop occurring.
        leaves = t.leaves()
        return ' '.join(leaves[1:])
    return winner

def answer_who_question(candidates,question,modules):

    parser = modules['parser']
    depParser = modules['depParser']
    lem = modules['lemmatizer']
    
    #process question
    qParseTree = parser.parse(word_tokenize(question)).next()[0]

    depTriplets = [list(parse.triples()) for parse in depParser.parse(word_tokenize(question))]
    qParseTree = lemmatize_tree(qParseTree,lem)
    [mainVerb,auxVerbs,subject] = getSubjectVerbs(depTriplets)
    mainVerb = list(mainVerb)
    mainVerb[0] = lem.lemmatize(mainVerb[0].lower(),'v')

    subject=list(subject)
    subject[0]=subject[0].lower()

    #process answers, and choose the one that has the most number of matching terms with the question. Then find answer within.
    #ansParseTree,winner = find_winning_candidate(candidates,qParseTree,parser,lem)
    winner = candidates[0]
    #print "Winner : "+winner
    tagged =  modules['ssTagger'].tag(winner)
    tagged = chunk_person(tagged)
    #fall-back method to answer who questions
    common = ""
    for t in tagged:
        if t[2]=='B-noun.person':
            if(t[0] not in question):
                if(not common and t[1]!='NNP'):
                    common = t[0]
                elif(t[1]=='NNP'):
                    return t[0]
                else:
                    pass
    if(common):
        return common

    return winner


def answer_what_question(candidates,question,modules):

    parser = modules['parser']
    depParser = modules['depParser']
    lem = modules['lemmatizer']
    #process question
    qParseTree = parser.parse(word_tokenize(question)).next()[0]

    qDepTriplets = [list(parse.triples()) for parse in depParser.parse(word_tokenize(question))] 
    qParseTree = lemmatize_tree(qParseTree,lem)
    [mainVerb,auxVerbs,subject] = getSubjectVerbs(qDepTriplets)
    mainVerb = list(mainVerb)
    mainVerb[0] = lem.lemmatize(mainVerb[0].lower(),'v')

    subject=list(subject)
    #subject[0]=subject[0].lower()

    #process answers, and choose the one that has the most number of matching terms with the question. Then find answer within.
    ansParseTree,winner = find_winning_candidate(candidates,qParseTree,parser,lem)
    ansDepTriplets = [list(parse.triples()) for parse in depParser.parse(word_tokenize(winner))] 
    #print "Winner : "+winner

    minH = 1000000
    subjectTree = None
    for t in ansParseTree.subtrees(lambda t: mainVerb[0] in t.leaves() and subject[0] in t.leaves()):
        if(t.height() < minH):
            minH = t.height()
            subjectTree = t
    if(subjectTree==None):
        return winner
    maxH = 100000

    verbTree = None

    for t in subjectTree.subtrees(lambda t : t.label()=='VP'):
        if(t.height() < maxH):
            maxH = t.height()
            verbTree = t 
    if(verbTree==None):
        verbTree = subjectTree
    ans = []
    maxH=-10000
    ansTree = None
    for t in verbTree.subtrees(lambda t:t.label()=='NP'):
        if(t.height()>maxH):
            maxH=t.height()
            ansTree = t
    if(ansTree!=None):
        leaves = ansTree.leaves()
        return ' '.join(leaves)
    return winner


def answer_why_question(candidates,question,modules):

    #candidates = ['Supriya ate a banana']
    parser = modules['parser']
    depParser = modules['depParser']
    lem = modules['lemmatizer']
    #process question
    qParseTree = parser.parse(word_tokenize(question)).next()[0]

    qDepTriplets = [list(parse.triples()) for parse in depParser.parse(word_tokenize(question))] 
    qParseTree = lemmatize_tree(qParseTree,lem)
    '''[mainVerb,auxVerbs,subject] = getSubjectVerbs(qDepTriplets)
    mainVerb = list(mainVerb)
    mainVerb[0] = lem.lemmatize(mainVerb[0].lower(),'v')

    subject=list(subject)
    subject[0]=subject[0].lower()'''

    #process answers, and choose the one that has the most number of matching terms with the question. Then find answer within.
    ansParseTree,winner = find_winning_candidate(candidates,qParseTree,parser,lem)
    #ansDepTriplets = [list(parse.triples()) for parse in depParser.parse(word_tokenize(winner))] 
    #print "Winner : "+winner
    if(winner.lower().find('because')>=0):
        return winner[winner.lower().find('because'):]
    elif(winner.lower().find('due to')>=0):
        ind = winner.lower().find('due to')
        return winner[ind:]
    else:
        return winner


def chunk_person(tagged):
    toRet = []
    cur=""
    for t in tagged:
        if t[2]=='B-noun.person' and cur=="":
            cur=t[0]
        elif t[2]=='I-noun.person' and cur:
            cur+=" "+t[0]
        else:
            if cur:
                toRet.append([cur,'NNP','B-noun.person'])
                cur=""
            else:
                toRet.append(t)
    return toRet


def lemmatize_tree(root,lem):
    if root == None:
        return root
    temp = None
    for child in root:
        if isinstance(child, Tree):
            child = lemmatize_tree(child,lem)
        else:
            if(root.label().startswith('V')):
                root[0] = lem.lemmatize(child,'v')
            #else:
                #root[0] = lem.lemmatize(child)
    return root


def find_winning_candidate(candidates,qParseTree,parser,lem):
    winner = None
    maxW = -1
    for candidate in candidates:
        tokens = word_tokenize(candidate)
        ans_words  = set(remove_stopwords(tokens))
        q_words = set(remove_stopwords(qParseTree.leaves()))
        matching = len(ans_words.intersection(q_words))
        if(matching>maxW):
            maxW = matching
            winner = candidate
    ansParseTree = parser.parse(word_tokenize(winner)).next()
    ansParseTree = ansParseTree[0]
    ansParseTree = lemmatize_tree(ansParseTree,lem)
    return ansParseTree,winner



