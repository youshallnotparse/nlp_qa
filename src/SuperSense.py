import socket
import sys
from nltk import word_tokenize

class SuperSenseTagger:
    
    def __init__(self,posTagger,port):
        self.pos = posTagger
        self.ssPort = int(port)
        self.server_address = ('localhost', self.ssPort)
    
    def tag(self,sentence):
        if sentence is None:
            return []
        inp = sentence
        if isinstance(inp,str) or isinstance(inp,unicode):
            inp = word_tokenize(inp)
        sock = self.createSocket()
        taggedInp = self.pos.tag(inp)
        message = "\n".join([str(t[0]) + "\t" + str(t[1]) for t in taggedInp])
        message = message.strip() + "\n\n"
        try:
            self.sendMessage(sock, message)
            dat = self.recvMessage(sock)
        finally:
            self.closeSocket(sock)
        
        lines = dat.split("\n")
        ans = [self.processLine(l) for l in lines if len(l) != 0]
        return ans
        
    def createSocket(self):
        # Create a TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(self.server_address)
        return sock
    
    def closeSocket(self,sock):
        if sock:
            sock.close()
        
    def sendMessage(self,sock,message):
        if sock and message is not None:
            sock.sendall(message)
    
    def recvMessage(self,sock):    
        ans = ""
        if sock:
            data = sock.recv(128)
            while len(data) > 0:
                ans += data
                data = sock.recv(128)
        return ans
    
    def processLine(self,line):
        tokens = line.strip().split()
        return (tokens[0],tokens[1],tokens[2])