from nltk import word_tokenize 
from nltk import Tree
from subprocess import check_output
import constants
import string

UNMOVE_FILE = 'MarkUnmv.sh'
TEMPFILE_PATH = '../tmp/tmp.txt'

ANSWER_CANDIDATES = {constants.NP_STRING,constants.PP_STRING,constants.SBAR_STRING} 

DELIMITER = u'_!@$%_'

def generateMediumQuestions(sentence,modules):
    parser = modules['parser']
    depParser = modules['depParser']
    lem = modules['lemmatizer']
    nerTagger = modules['nerTagger']
    posTagger = modules['posTagger']
    ssTagger = modules['ssTagger']
    antonyms = modules['antonyms']
    
    tokens = word_tokenize(sentence)
    
    parseTree = parser.parse(tokens).next()
    [parseTree,tokens] = checkSentenceStartPP(parseTree, tokens)
    sentence = " ".join(tokens)
    depTriplets = [list(parse.triples()) for parse in depParser.parse(tokens)]
    #print depTriplets
    [mainVerb,auxVerbs,subject] = getSubjectVerbs(depTriplets)
    if mainVerb == "" or subject == "":
        return []
    vp = getVPIndex(parseTree[0])
    if vp == -1:
        return []
    ssTags = ssTagger.tag(sentence)
    posTags = parseTree.pos()
    
    '''
    taggedSentence = posTagger.tag(tokens)
    modifiedTags = [(taggedSentence[i][0]+DELIMITER+str(i),taggedSentence[i][1]) for i in range(len(taggedSentence))]
    '''
    
    #newParseTree = parser.tagged_parse(modifiedTags).next()
    if isIllegalTree(parseTree):
        #print "Illegal tree"
        return []
    
    modifiedTree = markUnmovables(parseTree)
    
    candidates = [processCandidate(s,ssTags) for s in modifiedTree.subtrees(lambda t : t.label() in ANSWER_CANDIDATES)]
    
    #questions = []
    binaryQ = []
    binaryAntonymQ = []
    whoQ = []
    whatQ = []
    whenQ = []
    whereQ = []
    
    ans = deepCopyTokens(tokens)
    #Make a note of all the adjectives in the sentence
    adj = [i for i in range(len(ans)) if posTags[i][1] == constants.ADJECTIVE_STRING and ans[i] in antonyms]
    
    #Now start playin with the structure of the sentence
    if len(auxVerbs) > 0 or lem.lemmatize(mainVerb[0].lower(),'v') == constants.BE_BASE: 
        if len(auxVerbs) > 0:
            indices = [i for i in range(len(tokens)) if tokens[i] in auxVerbs and i >= vp]
        else:
            indices = [i for i in range(len(tokens)) if tokens[i] == mainVerb[0] and i >= vp]
        auxIndex = indices[0]
        auxVerb = tokens[auxIndex]
        if constants.PROPER_NOUN_STRING not in parseTree.pos()[0]:
            ans[0] = ans[0].lower()
        
        yesNoQues = [a for a in ans]
        candYesNoQues = [yesNoQues]
        for i in adj:
            candYesNoQues = candYesNoQues + [replaceWithAntonym(yesNoQues, i, antonyms)] 
        for i in range(len(candYesNoQues)):
            del candYesNoQues[i][auxIndex]
            candYesNoQues[i] = [auxVerb.title()] + candYesNoQues[i]
            addQuestionMark(candYesNoQues[i])
            if i == 0: 
                binaryQ.append(combine(candYesNoQues[i]))
            else:
                binaryAntonymQ.append(combine(candYesNoQues[i]))
        
        for c in candidates:
            whWord = getWhWord(c,depTriplets,subject)
            if whWord in {constants.WHO_STRING,constants.WHAT_STRING} and c[0].label() == constants.PP_STRING:
                candAns = removeAuxAndCandidate(ans,auxIndex,(c[1]+1,c[2]))
            else:
                candAns = removeAuxAndCandidate(ans,auxIndex,(c[1],c[2]))
            candAns = [whWord.title(), auxVerb] + candAns
            addQuestionMark(candAns)
            
            if whWord == constants.WHO_STRING:
                whoQ.append(combine(candAns))
            elif whWord == constants.WHEN_STRING:
                whenQ.append(combine(candAns))
            elif whWord == constants.WHERE_STRING:
                whereQ.append(combine(candAns))
            else:
                whatQ.append(combine(candAns))
            
    # Case of Do/Does
    else:
        indices = [i for i in range(len(tokens)) if tokens[i] == mainVerb[0] and i >= vp]
        index = indices[0]
        if constants.PROPER_NOUN_STRING not in parseTree.pos()[0]:
            ans[0] = ans[0].lower()
        
        if isPresentTense(mainVerb):
            if subject[0].lower() in constants.DO_SUBJECTS or subject[1] in constants.PLURAL_NOUNS:
                doVerb = u'do'
            else:
                doVerb = u'does'
        else:
            doVerb = u'did'
            
        yesNoQues = [a for a in ans]
        yesNoQues[index] = lem.lemmatize(yesNoQues[index],'v')
        
        candYesNoQues = [yesNoQues]
        for i in adj:
            candYesNoQues = candYesNoQues + [replaceWithAntonym(yesNoQues, i, antonyms)] 
        for i in range(len(candYesNoQues)):
            candYesNoQues[i] = [doVerb.title()] + candYesNoQues[i]
            addQuestionMark(candYesNoQues[i])
            
            if i == 0: 
                binaryQ.append(combine(candYesNoQues[i]))
            else:
                binaryAntonymQ.append(combine(candYesNoQues[i]))
                
        for c in candidates:
            whWord = getWhWord(c,depTriplets,subject)
            if isSubjectCandidate(c, subject)[0] == True:
                candAns = deepCopyTokens(ans)
                if whWord in {constants.WHO_STRING,constants.WHAT_STRING} and c[0].label() == constants.PP_STRING:
                    del candAns[c[1]+1:c[2]+1]
                else:
                    del candAns[c[1]:c[2]+1]
                candAns = [whWord.title()] + candAns
                addQuestionMark(candAns)
                
                if whWord == constants.WHO_STRING:
                    whoQ.append(combine(candAns))
                elif whWord == constants.WHEN_STRING:
                    whenQ.append(combine(candAns))
                elif whWord == constants.WHERE_STRING:
                    whereQ.append(combine(candAns))
                else:
                    whatQ.append(combine(candAns))
            
            
            else:
                candAns = deepCopyTokens(ans)
                candAns[index] = lem.lemmatize(candAns[index],'v')
                if whWord in {constants.WHO_STRING,constants.WHAT_STRING} and c[0].label() == constants.PP_STRING:
                    del candAns[c[1]+1:c[2]+1]
                else:
                    del candAns[c[1]:c[2]+1]
                candAns = [whWord.title(), doVerb] + candAns
                addQuestionMark(candAns)
                if whWord == constants.WHO_STRING:
                    whoQ.append(combine(candAns))
                elif whWord == constants.WHEN_STRING:
                    whenQ.append(combine(candAns))
                elif whWord == constants.WHERE_STRING:
                    whereQ.append(combine(candAns))
                else:
                    whatQ.append(combine(candAns))
                
                
    return [binaryQ,binaryAntonymQ,whoQ,whenQ,whereQ,whatQ]

def isIllegalTree(parseTree):
    return len(parseTree) != 1 or len(parseTree[0]) != 1 and parseTree[0].label() != 'S'

def markUnmovables(parseTree):
    with open(TEMPFILE_PATH,'w') as f:
        f.write(str(parseTree))
    modifiedTree = check_output("bash "+UNMOVE_FILE,shell = True)
    return annotateTreeLeaves(Tree.fromstring(modifiedTree.strip()))

def annotateTreeLeaves(t):
    pos = [l for l in t.treepositions('leaves')] 
    for i in range(len(pos)):
        t[pos[i]] = t[pos[i]] + DELIMITER + str(i)
    return t

def processCandidate(s,ssTags):
    leaves = s.leaves()
    low = int(leaves[0].split(DELIMITER)[1])
    high = int(leaves[len(leaves)-1].split(DELIMITER)[1])
    ss = [ssTags[i] for i in range(low,high+1) if ssTags[i][2].startswith('B-noun') or  ssTags[i][2].startswith('I-noun')]
    return(s,low,high,ss)
    
def getSubjectVerbs(depTriplets):
    subjVerb = [t for t in depTriplets[0] if constants.SUBJECT_STRING in t[1]]
    mainVerb = ""
    subject = ""
    auxVerbs = []
    if len(subjVerb) > 0:
        subject = subjVerb[0][2]
        if constants.VERB_STRING not in subjVerb[0][0][1]:
            copVerb = [t for t in depTriplets[0] if constants.COP_STRING in t[1]]
            if len(copVerb) > 0 :
                mainVerb = copVerb[0][2]
        else:
            mainVerb = subjVerb[0][0]
            auxVerbs += [t[2][0] for t in depTriplets[0] if constants.AUX_STRING in t[1] and t[0][0] == mainVerb[0]]
    
    return [mainVerb,auxVerbs,subject]    

def getVPIndex(parseTree):
    ct = 0
    for t in parseTree:
        if t.label() == constants.VP_STRING:
            return ct
        else:
            ct += len(t.leaves())
    return -1


def isSubjectCandidate(c,subj):
    ssTags = c[3]
    tags = c[0].pos()
    tags = {(t[0].split(DELIMITER)[0].lower(),t[1]) for t in tags}
    if subj[1] == constants.PERSONAL_PRONOUN_STRING:
        tags = {(t[0].split(DELIMITER)[0].lower(),t[1]) for t in tags}
        if (subj[0].lower(),subj[1]) in tags:
            return [True,subj]
    else:
        for s in ssTags:
            if s[0].lower() == subj[0].lower():
                return [True,s]
    return [False,None]

def getWhWord(c,depTriplets,subject): #c is the answer phrase
    subTree = c[0]
    if subTree.label() == constants.SBAR_STRING:
        return constants.WHAT_STRING
    
    elif subTree.label() == constants.PP_STRING:
        # get the preposition from the tree
        [prep,prepPos] = getPrepfromPP(subTree)
        # In case we fuck up somewhere, return What
        if prep == "":
            return constants.WHAT_STRING
        #print prep
        # Now that we have the preposition, try to find the head of the preposition phrase
        # Can use the dependency dependency triplets for this purpose. Case triplet gives the object of the PP
        prepNoun = [d[0] for d in depTriplets[0] if d[1] == constants.CASE_STRING and (d[2][0].lower(),d[2][1]) == prep ]
        # Backup for fuck up
        if len(prepNoun) == 0:
            return constants.WHAT_STRING
        else:
            prepNoun = prepNoun[0]
        
        # If PRP, then dont do anything, directly return Who :-)
        if prepNoun[1] == constants.PERSONAL_PRONOUN_STRING:
            return constants.WHO_STRING
        
        # Try to find the tag in the supersense tags
        noun = findInSuperSense(c[3], prepNoun)
        if noun == '':
            return constants.WHAT_STRING # Katt Gaya!
        tag = mapToTag(noun[2])
        if tag == constants.TAG_PERSON:
            return constants.WHO_STRING
        elif tag == constants.TAG_TIME:
            return constants.WHEN_STRING
        elif tag == constants.TAG_LOCATION and prep[0].lower() in {u'on',u'in',u'at',u'over',u'to'}:
            return constants.WHERE_STRING
        else:
            return constants.WHAT_STRING 
    
    elif subTree.label() == constants.NP_STRING:
        [isSubj,subjTag] =isSubjectCandidate(c, subject) 
        if isSubj == True:
            # Backup in case of screw up
            if subjTag == None:
                return constants.WHAT_STRING
            # It is a personal Pronoun
            elif subjTag[1] == constants.PERSONAL_PRONOUN_STRING:
                return constants.WHO_STRING
            # Backup in case of screw up
            if len(subjTag) == 2:
                return constants.WHAT_STRING
            # Map Super-sense tag to person , location etc
            tag = mapToTag(subjTag[2])
            # In case of Person, give Who
            if tag == constants.TAG_PERSON:
                return constants.WHO_STRING
            # In case of Time, give when
            elif tag == constants.TAG_TIME:
                return constants.WHEN_STRING
            # Jab Kahi pe kuch nahi tha, bas tu hi tha
            else:
                return constants.WHAT_STRING
        else:
            dObj = findDobjMatch(c, depTriplets)
            if dObj == None:
                return constants.WHAT_STRING
            
            if dObj[1] == constants.PERSONAL_PRONOUN_STRING:
                return constants.WHO_STRING
            tag = mapToTag(dObj[2])
            # In case of Person, give Who
            if tag == constants.TAG_PERSON:
                return constants.WHO_STRING
            # In case of Time, give when
            elif tag == constants.TAG_TIME:
                return constants.WHEN_STRING
            # Jab Kahi pe kuch nahi tha, bas tu hi tha
            else:
                return constants.WHAT_STRING
            
    return "Wh-Word"
def mergeCandidates(candidates):
    
    return

def deepCopyTokens(tokens):
    return [tokens[i] for i in range(len(tokens))]

def removeAuxAndCandidate(ans,auxIdx,bounds):
    ret = deepCopyTokens(ans)
    if auxIdx < bounds[0]:
        del ret[bounds[0]:bounds[1]+1]
        del ret[auxIdx]
    elif auxIdx > bounds[1]:
        del ret[auxIdx]
        del ret[bounds[0]:bounds[1]+1]
    else: 
        del ret[bounds[0]:bounds[1]+1]
    return ret
    
def addQuestionMark(ans):
    if ans[len(ans)-1] in string.punctuation:
        ans[len(ans)-1] = constants.QMARK
    else:
        ans += [constants.QMARK]
        
def combine(ans):
    ret = ""
    for s in ans:
        if s[0] in string.punctuation:
            ret += s
        else:
            ret += " "+s
    return ret.strip()

def isPresentTense(mainVerb):
    return mainVerb[1] in constants.PRESENT_VERB_FORMS 

def getPrepfromPP(tree):
    if not tree:
        return ""
    
    for t in tree:
        if t.label() == constants.PREPOSITION_STRING or t.label() == constants.TO_STRING:
            l = t.pos()[0]
            return [ (l[0].split(DELIMITER)[0].lower(),l[1]), l[0].split(DELIMITER)[1]]
    return ['',''] 

def findInSuperSense(ss,toFind):
    match = [s for s in ss if toFind[0].lower() == s[0].lower()]
    if len(match) == 0:
        return ''
    return match[0]

def mapToTag(readTag):
    rt = readTag.split('.')[1]
    if constants.SS_TAGS.has_key(rt):
        return constants.SS_TAGS[rt]
    else:
        return ''
   
def findDobjMatch(c,depTriplets):
    dObj = { t[2][0].lower():t[2] for t in depTriplets[0] if constants.DOBJECT_STRING in t[1]}
    if len(dObj) == 0:
        return None
    ssTags = c[3]
    ssT = [s for s in ssTags if s[0].lower() in dObj and  matchTuple(dObj[s[0].lower()], (s[0],s[1]))]
    if len(ssT) > 0:
        return ssT[0]
    tags = c[0].pos()
    tags = [(t[0].split(DELIMITER)[0].lower(),t[1]) for t in tags if t[1] == constants.PERSONAL_PRONOUN_STRING and t[0].split(DELIMITER)[0].lower() in dObj]
    if len(tags) > 0:
        return tags[0]
    else: 
        return None
    
def matchTuple(t1,t2):
    return t1[0] == t2[0] and t1[1][0:2] == t2[1][0:2]

def checkSentenceStartPP(parseTree,tokens):
    tree = parseTree[0]
    if len(tree) > 1:
        if tree[0].label() == constants.PP_STRING:
            pp = tree[0]
            pos = [l for l in pp.treepositions('leaves')]
            if len(pos) > 0:
                pp[pos[0]] = pp[pos[0]].lower()  
            last = tree[len(tree)-1]
            if last.label() in string.punctuation:
                del tree[len(tree)-1]
                tree.append(pp)
                tree.append(last)
            else:
                tree.append(pp)
            del tree[0]
            if tree[0].label() in string.punctuation:
                del tree[0]
            pos = [l for l in tree[0].treepositions('leaves')]
            if len(pos) > 0:
                tree[0][pos[0]] = tree[0][pos[0]].title()  
            tokens = tree.leaves()
    
    return [parseTree,tokens] 
    
def replaceWithAntonym(tokens,i,antonyms):
    candQues = deepCopyTokens(tokens)
    candQues[i] = antonyms[candQues[i]]
    return candQues