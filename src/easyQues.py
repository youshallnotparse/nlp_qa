from nltk import word_tokenize 
import string
import constants

BE_BASE = u'be'
DO_SUBJECTS = [u'i',u'we',u'they',u'you'] 

def generateEasyQuestion(sentence,modules):
    parser = modules['parser']
    depParser = modules['depParser']
    lem = modules['lemmatizer']
    
    tokens = word_tokenize(sentence)
    parseTree = parser.parse(tokens).next()
    if isIllegalTree(parseTree):
        print "Illegal tree"
        return
    
    parseTree = parseTree[0]
    depTriplets = [list(parse.triples()) for parse in depParser.parse(tokens)] 
    [mainVerb,auxVerbs,subject] = getSubjectVerbs(depTriplets)
    vp = getVPIndex(parseTree)
    if vp == -1:
        return ""
    
    ans = [tokens[i] for i in range(len(tokens))]
    if len(auxVerbs) > 0:
        indices = [i for i in range(len(tokens)) if tokens[i] in auxVerbs and i >= vp]
        index = indices[0]
        auxVerb = tokens[index]
        if constants.PROPER_NOUN_STRING not in parseTree.pos()[0]:
            ans[0] = ans[0].lower()
        del ans[index]
        ans = [auxVerb.title()] + ans
    
    elif lem.lemmatize(mainVerb[0].lower(),'v') == BE_BASE:
        indices = [i for i in range(len(tokens)) if tokens[i] == mainVerb[0] and i >= vp]
        index = indices[0]
        if constants.PROPER_NOUN_STRING not in parseTree.pos()[0]:
            ans[0] = ans[0].lower()
        del ans[index]
        ans = [mainVerb[0].title()] + ans
    
    else:
        indices = [i for i in range(len(tokens)) if tokens[i] == mainVerb[0] and i >= vp]
        index = indices[0]
        ans[index] = lem.lemmatize(ans[index],'v')
        if constants.PROPER_NOUN_STRING not in parseTree.pos()[0]:
            ans[0] = ans[0].lower()
        if isPresentTense(mainVerb):
            if subject[0].lower() in DO_SUBJECTS:
                ans = [u'do'.title()] + ans
            else:
                ans = [u'does'.title()] + ans
        else:
            ans = [u'did'.title()] + ans
        
    addQuestionMark(ans)
    return combine(ans)
    
def addQuestionMark(ans):
    if ans[len(ans)-1] in string.punctuation:
        ans[len(ans)-1] = constants.QMARK
    else:
        ans += [constants.QMARK]
        
def combine(ans):
    ret = ""
    for s in ans:
        if s[0] in string.punctuation:
            ret += s
        else:
            ret += " "+s
    return ret.strip()

def getVPIndex(parseTree):
    ct = 0
    for t in parseTree:
        if t.label() == constants.VP_STRING:
            return ct
        else:
            ct += len(t.leaves())
    return -1

def isPresentTense(mainVerb):
    return mainVerb[1] in constants.PRESENT_VERB_FORMS 

def getSubjectVerbs(depTriplets):
    subjVerb = [t for t in depTriplets[0] if constants.SUBJECT_STRING in t[1]]
    mainVerb = ""
    subject = ""
    auxVerbs = []
    if len(subjVerb) > 0:
        subject = subjVerb[0][2]
        if constants.VERB_STRING not in subjVerb[0][0][1]:
            copVerb = [t for t in depTriplets[0] if constants.COP_STRING in t[1]]
            if len(copVerb) > 0 :
                mainVerb = copVerb[0][2]
        else:
            mainVerb = subjVerb[0][0]
            auxVerbs += [t[2][0] for t in depTriplets[0] if constants.AUX_STRING in t[1] and t[0][0] == mainVerb[0]]
    
    return [mainVerb,auxVerbs,subject]

def isIllegalTree(parseTree):
    return len(parseTree) != 1 or len(parseTree[0]) != 1 and parseTree[0].label() != 'S'
    
