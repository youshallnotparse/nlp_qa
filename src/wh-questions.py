import os
import sys
from configureEnvironment import prepareEnvironment
from stanfordClasses import getStanfordParser, getStanfordNERTagger, getStanfordDependencyParser,\
    getStanfordPOSTagger
from nltk.stem.wordnet import WordNetLemmatizer
from easyQues import generateEasyQuestion
import subprocess
from nltk.tokenize.punkt import PunktSentenceTokenizer
#from nlp_qa.src.stanfordClasses import getStanfordNERTagger

def buildModules():
    parseModule = {
                   'posTagger':getStanfordPOSTagger(),
                   'parser':getStanfordParser(),
                   'depParser':getStanfordDependencyParser(),
                   'nerTagger':getStanfordNERTagger(),
                   'lemmatizer':WordNetLemmatizer()
                   }
    
    return parseModule

prepareEnvironment()
modules = buildModules()

parser = modules['parser']
tagger = modules['nerTagger']

line = "John saw Mary."
tokenizer = PunktSentenceTokenizer()
sent = tokenizer.tokenize(line)

for s in sent:
    s = s.rstrip('.')
    parseTree = list(parser.raw_parse(s))
    root = parseTree[0]
    tree = str(root).replace("\n","")
    tree = tree.replace("\t","")
    
f = open("tree.txt","w")
f.write(tree)

print tree

r = "rule"
rule = ""
for i in xrange(1,19):
    rule += "../"+r+str(i)+" "

#rule = "../rule1"
cmd = "bash ./tsurgeon.sh -s -treeFile ../tree.txt"+" "+rule
print cmd

os.chdir("stanford-tregex-2014-10-26")
#print os.getcwd()
#print os.listdir(os.getcwd())
output = subprocess.check_output(cmd,shell='True')
os.chdir('..')

print type(output)
print len(output)