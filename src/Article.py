import re

class Article:
    def __init__(self,lines):
        
        match = 0
        for i in range(len(lines)):
            if len(lines[i].strip()) == 0:
                continue
            else:
                self.title = lines[i].strip()
                match = i 
                break
        
        self.sections = []
        section = None
        for i in range(match+1,len(lines)):
            if len(lines[i].strip()) == 0:
                if section is not None:
                    self.sections.append(" ".join(section))
                    section = None
                continue 
            else:
                if section is None:
                    section = []
                
                patterns = [ "\.\.+", "/.*/", "\"", "-.RB-"]
                repl_strings = [ ".", "", "", ""]
                txt = lines[i].strip().encode("ascii", "ignore")
                for (p, s) in zip(patterns, repl_strings):
                    txt = re.sub(re.compile(p), s, txt)
                section.append(txt)
        if section is not None:
            self.sections.append(" ".join(section))
            section = None
            
        print len(self.sections)
        print self.title        