from nltk.tree import Tree


def contains_list(l1, l2):
    for l in l2:
        if l not in l1:
            return False
    return True


def get_subtree(root, words):
    if root == None:
        return root
    temp = None
    for child in root:
        if isinstance(child, Tree):
            temp = get_subtree(child, words)
            if temp != None:
                break
    if temp == None and contains_list(root.leaves(), words):
        temp = root
    return temp


def get_child(root, label, word=""):
    if root == None:
        return root    
    res = None
    for child in root:
        if isinstance(child, Tree):
            temp = get_child(child, label, word)
            if temp != None:
                return temp
    if root.label() == label:
        if word == "" or word in root.leaves():
            return root

    return None


def get_child_index(root, pos):
    if root == None:
        return root
    i = 0
    for child in root:
        if i == pos:
            return child
        i += 1
    return None


def get_sub_child(root, plabel, clabel):
    if root == None:
        return root
    for child in root:
        if isinstance(child, Tree):
            temp = get_sub_child(child, plabel, clabel)
            if temp != None:
                return temp
    if root.label() == plabel:
        for child in root:
            if child.label() == clabel:
                return root
    return None


def lemmatize_verbs(root):
    cond_str = "VB"
    if root == None:
        return root
    for child in root:
        if isinstance(child, Tree):
            child = get_subtree(child)