# -*- coding: utf-8 -*-
import sys
from configureEnvironment import prepareEnvironment
from configureEnvironment import getGlobalProps
from stanfordClasses import getStanfordParser, getStanfordDependencyParser,\
    getStanfordPOSTagger, getStanfordNERTagger
from nltk.stem.wordnet import WordNetLemmatizer
from easyQues import generateEasyQuestion
from mediumQuestions import generateMediumQuestions
from SuperSense import SuperSenseTagger
import time
from FSE import FSE
import cPickle as pickle
#from Article import Article
from nltk import sent_tokenize
import re
import string
from nltk import word_tokenize
import random
import math

ANTONYMS_FILE = 'pkl/antonyms.pkl'
NO_OF_SENTENCES = 35

def formatize(text, complete=False):
    '''
    1. Strip off 's and '
    2. Remove extra/unwanted chars
    3. Convert to lower case 
    
    TODO :
    1. Replace surnames with full names ??? 
    '''
    patterns = ["\n+", "\.\.+", "/.*/", "\"", "-.RB-"]
    repl_strings = [". ", ".", "", "", ""]
    if complete:
        patterns.append("["+string.punctuation+"]")
        repl_strings.append("")
    for (p, s) in zip(patterns, repl_strings):
        text = re.sub(re.compile(p), s, text)
    return text

#from multiprocessing import Process

#nprocs = 6



def filter(s):
    
    puncs = set(list(string.punctuation))
    puncs.remove(".")
    puncs.remove(",")
    puncs.remove("(")
    puncs.remove(")")
    puncs.remove("$")
    puncs.remove("-")
    
    #print puncs
    
    for c in s:
        if c in puncs:
            return True
    
    return False

def main(argv):
    
    reload(sys)
    sys.setdefaultencoding('utf8')
    
    
    if len(argv) != 3:
        print "Incorrect Parameters to Program."
        print "Expected syntax : ask <FILENAME> <NUM_QUESTIONS>"
        sys.exit()
    
    articleFile = argv[1]
    
    try:
        questionCount = int(argv[2])
    except ValueError:
        print "How to generate " + str(argv[2]) + " number of questions?"
        sys.exit()
     
    try:
        article = open(articleFile,"r").read().encode("ascii", "ignore")
        article = formatize(article)
            
            #article = Article(f.readlines())
    except IOError:
        print "How to generate questions from a file that does not exist?"
        sys.exit()
    
    #t1 = time.time()
    prepareEnvironment()
    parseModules = buildModules()
    
    questions = []
    
    sentences =  sent_tokenize(article)
    
    temp = []
    for s in sentences:
        #s = "she was (born in xyz) oh yeah"
        try:
            i = s.index("(")
            j = s.index(")")
            fi = s[:i]
            la = s[j+1:]
            s = fi + la
            temp.append(s)
        except:
            temp.append(s)
        
    sentences = temp
    sentences = [s for s in sentences if filter(s)==False ]
    sentences = [s for s in sentences if len(word_tokenize(s)) <= 50]
    sentences = [s for s in sentences if len(word_tokenize(s)) >= 5]
    
    
    sentences = sentences[:NO_OF_SENTENCES]
    
    binaryQ = []
    binaryAntonymQ = []
    whoQ = []
    whenQ = []
    whereQ = []
    whatQ = []
    
    #print "starting..."
    for s in sentences:
        #t3 = time.time()
        #print s
        if s.count(",")>0:
            sen = parseModules['fse'].split(s)
        else:
            sen = [s]
        for se in sen:
            try:
                kool = generateMediumQuestions(se, parseModules)
            except:
                continue
            
            if kool==None:
                continue
            if len(kool) > 0:
                binaryQ += kool[0]
                binaryAntonymQ += kool[1]
                whoQ += kool[2]
                whenQ += kool[3]
                whereQ += kool[4]
                whatQ += kool[5]
        #t4 = time.time()
        #print t4-t3
    
    #output = open("output.txt","w")
    
    
    #t2 = time.time()
    #print t2-t1
    
    binaryQ = [q for q in binaryQ if len(q.split())>5]
    binaryAntonymQ = [q for q in binaryAntonymQ if len(q.split())>5]
    whoQ = [q for q in whoQ if len(q.split())>5]
    whenQ = [q for q in whenQ if len(q.split())>5]
    whereQ = [q for q in whereQ if len(q.split())>5]
    whatQ = [q for q in whatQ if len(q.split())>5]
    
    random.shuffle(binaryQ)
    random.shuffle(binaryAntonymQ)
    random.shuffle(whoQ)
    random.shuffle(whenQ)
    random.shuffle(whereQ)
    random.shuffle(whatQ)
    
    b1 = int(math.floor(0.2*questionCount))
    b2 = 0
    b3 = int(math.floor(0.2*questionCount))
    b4 = int(math.floor(0.3*questionCount))
    b5 = int(math.floor(0.2*questionCount))
    b6 = int(math.floor(0.1*questionCount))
    
    
    ctr = 0
    for q in binaryQ:
        if ctr!= b1:
            questions.append(q)
            ctr+=1
        else:
            break
        
    ctr = 0
    for q in binaryAntonymQ:
        if ctr!= b2:
            questions.append(q)
            ctr+=1
        else:
            break
        
    ctr = 0
    for q in whoQ:
        if ctr!= b3:
            questions.append(q)
            ctr+=1
        else:
            break
        
    ctr = 0
    for q in whenQ:
        if ctr!= b4:
            questions.append(q)
            ctr+=1
        else:
            break
        
    ctr = 0
    for q in whereQ:
        if ctr!= b5:
            questions.append(q)
            ctr+=1
        else:
            break
        
    ctr = 0
    for q in whatQ:
        if ctr!= b6:
            questions.append(q)
            ctr+=1
        else:
            break
    
    left = questionCount - len(questions)
    if left > 0:
        combinedList = whoQ + whenQ + whereQ + binaryQ
        random.shuffle(combinedList)
        for q in combinedList:
            if left == 0:
                break
            if q in questions:
                continue
            else:
                questions.append(q)
                left -= 1
    if left > 0:
        combinedList = binaryAntonymQ
        random.shuffle(combinedList)
        for q in combinedList:
            if left == 0:
                break
            if q in questions:
                continue
            else:
                questions.append(q)
                left -= 1
    if left > 0:
        for q in whatQ:
            if left == 0:
                break
            if q in questions:
                continue
            else:
                questions.append(q)
                left -= 1
    
    random.shuffle(questions)
    
    if len(questions) == 0:
        print "What came first, the chicken or the egg?"
        return
    
    for q in questions:
        print q

def buildModules(isAnswer=False):
    parseModule = {
                   'posTagger':getStanfordPOSTagger(),
                   'parser':getStanfordParser(),
                   'depParser':getStanfordDependencyParser(),
                   'lemmatizer':WordNetLemmatizer(),
                   'nerTagger':getStanfordNERTagger()
                   }
    parseModule['ssTagger'] = SuperSenseTagger(parseModule['posTagger'],getGlobalProps()['supersensePort'])
    if isAnswer != True:
        parseModule['fse'] = FSE(getGlobalProps()['fsePort'])
    with open(ANTONYMS_FILE,'rb') as f:
        parseModule['antonyms'] = pickle.load(f)
    return parseModule

def makeQuestions(txt,parseModules,fname):
    #print "Generating section " + str(fname)
    questions = []
    sentences =  sent_tokenize(txt)
    print sentences
    for s in sentences:
        q = generateMediumQuestions(s, parseModules)
        if len(q) > 0:
            questions += q
    #return questions
    with open("med.txt","w") as f:
        f.write(str(questions))

if __name__ == "__main__":
    start = time.time()
    #sys.argv.append('../data/a1.txt')
    #sys.argv.append('10')
    try:
        main(sys.argv)
    except:
        print "Hello, can you hear me?"
        
    print str(time.time()-start)